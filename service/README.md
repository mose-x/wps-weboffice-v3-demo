# WebOffice 开放平台文档接入回调示例

## 依赖

JDK 13+

## 使用

假设您使用的是 Linux 系统，并且安装好了 JDK 13 和 Maven

最简单的流程: 引入pom -> 实现接口 -> 部署

1. 引入项目:

     ~~~xml
    
    <dependency>
        <groupId>cn.ljserver.tool</groupId>
        <artifactId>web-office-v3</artifactId>
        <version>3.1.0</version>
    </dependency>
     ~~~

2. 使用的时候,尽量将sdk的source也拉下来,里面有很详细的注释

3. 至于改造sdk,其实完全没必要,因为已经完成了官方所有接口,你只需引入sdk,实现你想要的方法即可

[SDK-git地址](https://gitee.com/mose-x/wps-weboffice-sdk-v3)

## 说明

WebOffice Java SDK 中需要接入方实现的接口有：

- ExtendCapacityService # 扩展能力接口，包括历史版本、重命名等所有额外扩展功能
- MultiPhaseFileStorageService # 文档三阶段保存接口，非必须，且与 SinglePhaseFileStorageService 互斥，实现一个即可
- PreviewService # **预览服务接口，**必须实现**，包括获取文档信息、下载地址、当前用户权限接口**
- SinglePhaseFileStorageService # 文档保存接口，非必须，且与 MultiPhaseFileStorageService 互斥，实现一个即可
- UserService # 获取用户信息，非必须

Demo 中全部实现了，实际项目中可以按需实现。

## 其它: 
1. 使用SDK,则只需要实现相关接口,即,你只需要关心输入输出,其它的由SDK处理就行

2. 文中使用的是JPA,比较简单,以及使用了大量的lambda,实际使用,你可以按照你喜欢的或者是自己实际项目中的框架,随便更换

3. 如果想使用转换服务,则需要实现文档转换服务接口,具体请参考文档

4. 自用建议将项目中的token让前端通过wps平台传递回来,做统一鉴权,另外,通过由预览的appid,secret等配置自定义的token,实现更安全的鉴权

5. wps回传参数,均在头部中,详情参考HeaderUtils下的所有get方法

更多文档请参考：

[WebOffice 开放平台-回调配置](https://solution.wps.cn/docs/callback/summary.html)

