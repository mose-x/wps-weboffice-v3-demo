package cn.ljserver.project.entity;

import cn.ljserver.tool.weboffice.v3.model.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "t_user")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String nickname;

    private String avatar;

    /**
     * 转换为适用web office的UserInfo
     */
    public UserInfo toUserInfo() {
        return UserInfo.builder()
                .id(String.valueOf(this.id))
                .name(this.nickname)
                .avatarUrl(this.avatar)
                .build();
    }
}
