package cn.ljserver.project.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Builder
@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class FileId implements Serializable {
    private String id;
    private int version = 1;

    /**
     * 获取新版本
     */
    public FileId copyForNewVersion() {
        return new FileId(id, version + 1);
    }
}
