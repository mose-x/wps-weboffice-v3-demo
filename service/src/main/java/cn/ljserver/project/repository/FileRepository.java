package cn.ljserver.project.repository;

import cn.ljserver.project.entity.File;
import cn.ljserver.project.entity.FileId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileRepository extends CrudRepository<File, FileId> {

    // 通过id查找，并且通过version倒序（这个倒叙是官方要求的，必须的）
    List<File> findByIdIdOrderByIdVersionDesc(String id);
}
