package cn.ljserver.project.service;

import cn.ljserver.project.entity.User;
import cn.ljserver.project.repository.UserRepository;
import cn.ljserver.tool.weboffice.v3.exception.InvalidToken;
import cn.ljserver.tool.weboffice.v3.util.HeaderUtils;
import cn.ljserver.tool.weboffice.v3.util.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * user service
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    /**
     * 通过token获取用户信息
     * <p>
     * 这里是将token中的id直接当成用户id，实际使用，应该是token需要在拦截器中做鉴权
     * <p>
     * 而userid应该是从token中解码来的，或者是 通过 HeaderUtils.getUserQuery 来获取，前提是，前端传递了此值
     * <p>
     * 即前端传递了 初始化了 customArgs 参数并赋值
     * <br>
     * <a href="https://solution.wps.cn/docs/web/quick-start.html#%E6%AD%A5%E9%AA%A4-3-%E5%88%9D%E5%A7%8B%E5%8C%96">前端初始化赋值，详见官网</a>
     */
    public User fetchUserByToken() {
        return Optional.of(RequestUtils.getCurrentRequest())
                .map(HeaderUtils::getWebOfficeToken)
                .map(Long::parseLong)
                .flatMap(uid -> userRepository.findById(uid))
                .orElseThrow(InvalidToken::new);
    }
}
