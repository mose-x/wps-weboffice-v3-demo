package cn.ljserver.project.service;

import cn.ljserver.tool.weboffice.v3.exception.FileUploadNotComplete;
import cn.ljserver.tool.weboffice.v3.util.FileUtils;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 七牛文件上传
 */
public class QnFileService {

    // 创建上传token
    static final String accessKey = "xxxxxxxxx";
    static final String secretKey = "xxxxxxxxxxxxxxx";
    static final String bucket = "xxxxxx";
    // 注意下面的配置的 两个 “/” 别弄错了
    static final String directory = "xxxx/";
    static final String domain = "https://file.xxx.cn/";

    // 上传文件
    public static String upload(MultipartFile file) {
        if (file.isEmpty()) {
            throw new RuntimeException("文件是空的！");
        }

        // 文件名称
        String originalFilename = file.getOriginalFilename();

        // 构造文件目录和文件名
        assert originalFilename != null;
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        String fileKey = directory + FileUtils.uuid() + suffix;

        // 上传文件
        try {
            uploadManager().put(file.getInputStream(), fileKey, upToken(), null, null);
        } catch (IOException e) {
            throw new FileUploadNotComplete();
        }

        // 返回文件url
        // --------------------------------
        // 这里不应该直接URL，而是不带domain的uri方便后续迁移等等
        // 为了方便，就这么弄了
        // --------------------------------
        return domain + fileKey;
    }

    // 上传文件
    public static String upload(byte[] content, String suffix) {
        if (content.length == 0) {
            throw new RuntimeException("文件是空的！");
        }

        // 构造文件目录和文件名
        String fileKey = directory + FileUtils.uuid() + "." + suffix;

        // 上传文件
        try {
            uploadManager().put(content, fileKey, upToken());
        } catch (IOException e) {
            throw new FileUploadNotComplete();
        }

        // 返回文件url
        // --------------------------------
        // 这里不应该直接URL，而是不带domain的uri方便后续迁移等等
        // 为了方便，就这么弄了
        // --------------------------------
        return domain + fileKey;
    }

    private static String upToken(){
        Auth auth = Auth.create(accessKey, secretKey);
        return auth.uploadToken(bucket);
    }

    private static UploadManager uploadManager(){
        // 设置上传配置，Region要与存储空间所属的存储区域保持一致
        Configuration cfg = new Configuration(Region.createWithRegionId("z2"));
        // 创建上传管理器
        return new UploadManager(cfg);
    }
}
