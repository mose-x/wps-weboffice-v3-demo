package cn.ljserver.project.service;

import cn.ljserver.project.entity.File;
import cn.ljserver.project.entity.FileId;
import cn.ljserver.project.repository.FileRepository;
import cn.ljserver.tool.weboffice.v3.exception.FileNotExist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * 文件服务
 */
@Service
public class FileService {
    @Autowired
    private FileRepository fileRepository;

    public FileRepository repository() {
        return fileRepository;
    }

    /**
     * 通过文件id获取文件信息，获取不到，返回异常
     *
     * @param fileId 文件id
     */
    public File fetchFile(String fileId) {
        return Optional.ofNullable(fileId)
                .map(this::findById)
                .map(Collection::stream)
                .flatMap(s -> s.max(Comparator.comparingInt(f -> f.getId().getVersion())))
                .orElseThrow(FileNotExist::new);
    }

    // 获取单个版本
    public File fetchFileVersion(String fileId, int version) {
        return Optional.ofNullable(fileId)
                .map(id -> FileId.builder().id(fileId).version(version).build())
                .flatMap(repository()::findById)
                .orElseThrow(FileNotExist::new);
    }

    /**
     * 通过文件id获取文件信息
     *
     * @param fileId 文件id
     */
    public List<File> findById(String fileId) {
        return fileRepository.findByIdIdOrderByIdVersionDesc(fileId);
    }

}
