package cn.ljserver.project.webOfficeImpl;

import cn.ljserver.project.entity.User;
import cn.ljserver.project.repository.UserRepository;
import cn.ljserver.tool.weboffice.v3.exception.InvalidArgument;
import cn.ljserver.tool.weboffice.v3.model.UserInfo;
import cn.ljserver.tool.weboffice.v3.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * 用户信息接口实现
 * <br>
 * <a href="https://solution.wps.cn/docs/callback/user.html">-详见官方文档-</a>
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    /**
     * 获取用户信息
     *
     * @param userIds 用户ID列表
     * @return 用户信息列表
     */
    @Override
    public List<UserInfo> fetchUsers(List<String> userIds) {
        // 看着这里写这么多，感觉有点复杂
        // 其实就是获取到 userIds，然后到数据库中拿出对应的user list
        // 然后构造成userInfo list 返回即可
        return Optional.ofNullable(userIds)
                .map(List::stream)
                .map(s -> s.map(Long::parseLong))
                .map(s -> s.collect(Collectors.toList()))
                .map(s -> userRepository.findAllById(s))
                .map(s -> StreamSupport.stream(s.spliterator(), false))
                .map(s -> s.map(User::toUserInfo))
                .map(s -> s.collect(Collectors.toList()))
                .orElseThrow(InvalidArgument::new);
    }
}
