package cn.ljserver.project.webOfficeImpl;

import cn.ljserver.project.entity.File;
import cn.ljserver.project.service.FileService;
import cn.ljserver.tool.weboffice.v3.exception.FileNotExist;
import cn.ljserver.tool.weboffice.v3.exception.FileVersionNotExist;
import cn.ljserver.tool.weboffice.v3.model.DownloadInfo;
import cn.ljserver.tool.weboffice.v3.model.FileInfo;
import cn.ljserver.tool.weboffice.v3.model.Notify;
import cn.ljserver.tool.weboffice.v3.model.Watermark;
import cn.ljserver.tool.weboffice.v3.service.ExtendCapacityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * 扩展能力实现
 * <p>
 * <a href="https://solution.wps.cn/docs/callback/extend.html">-详见官方文档-</a>
 */
@Service
public class ExtendCapacityServiceImpl implements ExtendCapacityService {

    private static final Logger log = LoggerFactory.getLogger(ExtendCapacityServiceImpl.class);
    @Autowired
    private FileService fileService;

    /**
     * 文件重命名
     */
    @Override
    public void renameFile(String fileId, String name) {
        // 逻辑解释: 先查询是否有该文件对象,没有,抛出异常
        // 有则 更新名称后,执行save动作
        Optional.ofNullable(fileService.fetchFile(fileId))
                .ifPresentOrElse(
                        f -> {
                            f.setName(name);
                            fileService.repository().save(f);
                        },
                        FileNotExist::new
                );
    }

    /**
     * 获取文件版本列表
     */
    @Override
    public List<FileInfo> fileVersions(String fileId, int offset, int limit) {
        // 通过文件id查询出来文件版本列表,然后将File对象转换为FileInfo给WPS服务
        return Optional.ofNullable(fileId)
                .map(fileService::findById)
                .map(List::stream)
                .map(s -> StreamSupport.stream(s.spliterator(), false))
                .map(s -> s.map(File::toFileInfo))
                .map(s -> s.collect(Collectors.toList()))
                .orElseThrow(FileNotExist::new);
    }

    /**
     * 获取文件版本详情
     */
    @Override
    public FileInfo fileVersion(String fileId, int version) {
        // 通过文件id和版本号查询出来文件版本,然后将File对象转换为FileInfo给WPS服务
        return Optional.ofNullable(fileService.fetchFileVersion(fileId, version))
                .map(File::toFileInfo)
                .orElseThrow(FileNotExist::new);
    }

    /**
     * 获取文件版本下载地址<br>
     * 主要就是url 字段
     */
    @Override
    public DownloadInfo fileVersionDownload(String fileId, int version) {
        // 通过文件id和版本号查询出来文件版本,然后将FileUrl返回
        return Optional.ofNullable(fileService.fetchFileVersion(fileId, version))
                .map(f -> DownloadInfo.builder()
                        .url(String.format(f.getUrl()))
                        .build())
                .orElseThrow(FileVersionNotExist::new);
    }

    /**
     * 获取文件水印
     * <p>
     * 官方文档有说明，但是没有给出具体示例，所以这里直接返回默认水印
     * <p>
     * 正式 应用 才生效
     * <p>
     * 测试 应用 会被替换为官方默认的水印
     */
    @Override
    public Watermark fileWatermark(String fileId) {
        return Watermark.builder()
                .type(Watermark.Type.TEXT)
                .value("this a test" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                .fillStyle(Watermark.FillStyle.builder().alpha(0.6).red(192).green(192).blue(192).build())
                .rotate(-0.7853982)
                .horizontal(50)
                .vertical(100)
                .font("bold 20px Serif")
                .build();
    }

    /**
     * 消息通知
     * <p>
     * 要想实现,结合官网和接口去玩吧
     */
    @Override
    public void notify(Notify notify) {
        log.info("notify: {}", notify);
    }
}
