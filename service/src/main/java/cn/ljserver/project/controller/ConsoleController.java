package cn.ljserver.project.controller;

import cn.ljserver.project.config.FileSuffixCache;
import cn.ljserver.project.service.QnFileService;
import cn.ljserver.tool.weboffice.v3.exception.FileNotExist;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("/console")
public class ConsoleController {

    @PutMapping("/upload")
    public String upload(MultipartFile file) {
        return QnFileService.upload(file);
    }

    @PutMapping(value = "/upload/{file_id}")
    public String upload(@PathVariable("file_id") String fileId, @RequestBody byte[] content) {
        // ATTENTION a dirty version is written into storage
        if (!FileSuffixCache.suffixMap.containsKey(fileId)) throw new FileNotExist();
        // 从缓存中获取当前需要上传文件的文件类型
        String suffix = FileSuffixCache.suffixMap.get(fileId);
        return QnFileService.upload(content, suffix);
    }

}
