package cn.ljserver.project.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件后缀缓存，正真使用，需要替换为redis
 */
@Slf4j
@Component
public class FileSuffixCache {
    public static Map<String, String> suffixMap = new HashMap<>();

    @PostConstruct
    public void init() {
        log.info("init a new suffixMap ...");
        suffixMap = new HashMap<>();
    }

    @PreDestroy
    public void destroy() {
        log.info("destroyed the old suffixMap ...");
        suffixMap = null;
    }
}
