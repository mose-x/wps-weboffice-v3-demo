### web office 

该目录下存放的是服务端以及前端demo代码，包括：
1. 系统中的文件,用户 简单的代码逻辑,用jpa实现,你也可以换成如mp等
2. 在service中的webOfficeImpl目录,是实现webOffice的示例代码
   - 说白了,就是通过接口参数,返回web office需要的对象即可
3. 在web中,目前给了一个最简单的html案例,配上appid,系统中文件id,以及token给一个用户id,即可实现预览等
   - 这里的前提是,你要在wps开放平台申请开发者账号,并且申请了预览服务的appid
   - 然后将sdk集成到你的项目中
   - 部署项目
   - 配置wps回调
   - 打开相关的/你需要实现的接口
4. 最简单的测试方式就是,将本demo的service配置好后,部署到你的服务,然后配置回调地址,然后在index.html配置上正确的参数,即可实现对接完成
   - 将配置文件中的spring.jpa.hibernate.ddl-auto设置为create,即 spring.jpa.hibernate.ddl-auto=create,第一次启动后,会自动创建数据库表
   - 第一次启动后,后续需要设置为 spring.jpa.hibernate.ddl-auto=update,否则会报错,当更新字段后,会自动更新数据库

5. 示例

---

https://qnfile.ljserver.cn/weboffice/docx.html

---

https://qnfile.ljserver.cn/weboffice/pptx.html

---

https://qnfile.ljserver.cn/weboffice/xlsx.html

---

https://qnfile.ljserver.cn/weboffice/pdf.html
